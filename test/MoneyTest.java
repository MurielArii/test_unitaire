/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author raya
 */
public class MoneyTest {

    private Money m12CHF;
    private Money m14CHF;
    private Money f12CHF;
    private Money f7USD;

    public MoneyTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        m12CHF = new Money(12, "CHF");
        m14CHF = new Money(14, "CHF");
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
     public void testEquals(){
                 assertTrue(!m12CHF.equals(null));
                 assertEquals(m12CHF,m12CHF);
                 assertEquals(m12CHF,new Money(12,"CHF"));
                 assertTrue(!m12CHF.equals(m14CHF));

     }
     
     @Test
      public void testSimpleAdd(){
        Money expected = new Money(26,"CHF");
        Money result = (Money) m12CHF.add(m14CHF);
        assertTrue(expected.equals(result));
    }
}
